var numArr = [];

function xoaArr(){
    numArr = [];
    document.querySelector("#number").innerHTML = numArr;
    document.querySelector("#soN").focus();

}
function themSo(){
    num = document.querySelector("#soN").value;
    numArr.push(num);
    console.log(numArr);
    document.querySelector("#soN").value = "";
    document.querySelector("#soN").focus();
    document.querySelector("#number").innerHTML = numArr;
}

// Close other expand when click on one expand
$(document).ready(function(){
    $(".collapse").on("show.bs.collapse", function(){
        var index = $(this).attr("id");
        $(".collapse").each(function(){
            if($(this).attr("id") != index){
                $(this).collapse("hide");
            }
        });
    });
});

// Bai 1
function tinhTongDuong(){
    var sum = 0;
    numArr.forEach(element => {
         if(element >= 0){
            sum += parseInt(element);
         } 
    });
    document.querySelector("#tongDuong").innerHTML = sum;
}

// Bai 2
function demSoDuong(){
    var count = 0;
    numArr.forEach(element => {
        if(element >= 0){
            count++;
        }
    });
    document.querySelector("#demSoDuong").innerHTML = count;
}

// Bai 3
function timSoNhoNhat(){
    var min = numArr[0];
    numArr.forEach(element => {
        if(element < min){
            min = element;
        }
    });
    document.querySelector("#soNhoNhat").innerHTML = min;
}

// Bai 4
function timSoDuongNhoNhat(){
    var min = numArr[0];
    numArr.forEach(element => {
        if(element >= 0 && element < min){
            min = element;
        }
    });
    document.querySelector("#soDuongNhoNhat").innerHTML = min;
}

// Bai 5
function timSoChanCuoiCung(){
    var soChan = -1;
    numArr.forEach(element => {
        if(element % 2 == 0){
            soChan = element;
        }
    });
    document.querySelector("#soChanCuoiCung").innerHTML = soChan;
}

// Bai 6
function doiCho(){
    var v1 = document.querySelector("#v1").value;
    var v2 = document.querySelector("#v2").value;
    var temp = numArr[v1];
    numArr[v1] = numArr[v2];
    numArr[v2] = temp;
    document.querySelector("#mangSauDoi").innerHTML = numArr;
}

// Bai 7
function sapXepTang(){
    numArr.sort(function(a, b){return a - b});
    document.querySelector("#mangSort").innerHTML = numArr;
}

// Bai 8
function timSNT(){
    var snt = -1;
    for(var i = 0; i < numArr.length; i++){
        if(kiemTraSNT(numArr[i])){
            snt = numArr[i];
            break;
        }
    }
    document.querySelector("#soNT").innerHTML = snt;
}

function kiemTraSNT(n){
    if(n < 2){
        return false;
    }
    for(var i = 2; i <= Math.sqrt(n); i++){
        if(n % i == 0){
            return false;
        }
    }
    return true;
}

// Bai 9
var realNumArr = [];
function themSoNew(){
    num = document.querySelector("#soNNew").value;
    realNumArr.push(num);
    console.log(realNumArr);
    document.querySelector("#soNNew").value = "";
    document.querySelector("#soNNew").focus();
    document.querySelector("#mangSoThuc").innerHTML = realNumArr;
}

function timSoNguyen(){
    var countNguyen = 0;
    realNumArr.forEach(element => {
        if(Number.isInteger(parseFloat(element))){
            countNguyen++;
        }
    });
    document.querySelector("#soNguyen").innerHTML = countNguyen;
}

// Bai 10
function soSanh(){
    var countAm = 0;
    var countDuong = 0;
    for(var i =0 ; i<numArr.length ; i++){
        if(numArr[i] < 0){
            countAm++;
        }else if(numArr[i] > 0){
            countDuong++;
        }
    }
    if(countAm > countDuong){
        document.querySelector("#soSanh").innerHTML = "Số âm > số dương";
    }else if(countAm < countDuong){
        document.querySelector("#soSanh").innerHTML = "Số dương > số âm";
    }else{
        document.querySelector("#soSanh").innerHTML = "Số âm = số dương";
    }
}